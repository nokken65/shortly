module.exports = {
  content: ['./app/**/*.{js,ts,jsx,tsx}'],
  theme: {
    screens: {
      Mobile: '375px',
      Desktop: '1440px',
    },
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      white: '#fff',
      black: '#000',
      cyan: 'hsl(180, 66%, 49%)',
      red: 'hsl(0, 87%, 67%)',
      gray: 'hsl(0, 0%, 75%)',
      'dark-violet': 'hsl(257, 27%, 26%)',
      'grayish-violet': 'hsl(257, 7%, 63%)',
      'very-dark-blue': 'hsl(255, 11%, 22%)',
      'very-dark-violet': 'hsl(260, 8%, 14%)',
      'very-light-gray': 'hsl(230, 25%, 95%)',
    },
    extend: {},
  },
  plugins: [],
};
