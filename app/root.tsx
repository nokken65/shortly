import type { LinksFunction, MetaFunction } from '@remix-run/node';
import {
  Links,
  LiveReload,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
} from '@remix-run/react';

import tailwindStyleUrl from '~/tailwind.css';
import fontsStyleUrl from '~/styles/_fonts.css';
import indexStyleUrl from '~/styles/index.css';

const meta: MetaFunction = () => ({
  charset: 'utf-8',
  title: 'Shortly',
  viewport: 'width=device-width,initial-scale=1',
});

const links: LinksFunction = () => [
  { rel: 'stylesheet', href: tailwindStyleUrl },
  { rel: 'stylesheet', href: fontsStyleUrl },
  { rel: 'stylesheet', href: indexStyleUrl },
];

const App = () => {
  return (
    <html lang="en">
      <head>
        <Meta />
        <Links />
      </head>
      <body>
        <Outlet />
        <ScrollRestoration />
        <Scripts />
        <LiveReload />
      </body>
    </html>
  );
};

export { meta, links };
export default App;
