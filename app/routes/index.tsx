import { CallToAction } from '~/components/CallToAction';
import { Features } from '~/components/Features';
import { Footer } from '~/components/Footer';
import { Header } from '~/components/Header';
import { Hero } from '~/components/Hero';

export default function Index() {
  return (
    <div className="flex flex-col items-center justify-start w-full h-full">
      <Header />
      <Hero />
      <div className="w-full h-full bg-very-light-gray">
        <Features />
      </div>
      <CallToAction />
      <Footer />
    </div>
  );
}
