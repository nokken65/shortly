import { Card } from '../Card';

export const Features = () => {
  return (
    <section className="container flex flex-col items-center w-full pt-20 pb-20 ml-auto mr-auto">
      <div className="max-w-sm text-center">
        <h2 className="mb-3 text-4xl font-bold text-very-dark-blue">
          Advanced Statistic
        </h2>
        <p className="text-grayish-violet">
          Track how your links are performing across the web with our advanced
          statistic dashboard.
        </p>
      </div>
      <div className="mt-20 flex relative gap-8 before:content-[''] before:absolute before:top-1/2 before:h-2 before:w-full before:bg-cyan">
        <Card
          title="Brand Recognition"
          description="Boost your brand recognition with each click. Generic links dont't mean a thing. Branded links help instil confidence in your content."
          imgUrl="/images/icon-brand-recognition.svg"
        />
        <Card
          title="Detailed Records"
          description="Boost your brand recognition with each click. Generic links dont't mean a thing. Branded links help instil confidence in your content."
          imgUrl="/images/icon-detailed-records.svg"
          className="mt-10"
        />
        <Card
          title="Fully customizable"
          description="Boost your brand recognition with each click. Generic links dont't mean a thing. Branded links help instil confidence in your content."
          imgUrl="/images/icon-fully-customizable.svg"
          className="mt-20"
        />
      </div>
    </section>
  );
};
