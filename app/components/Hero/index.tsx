import Button from '../Button';

export const Hero = () => {
  return (
    <div className="container relative flex justify-center w-full h-full mt-8 mb-20">
      <div className="flex flex-col gap-2 mt-16 mb-16">
        <h1 className="text-6xl font-bold leading-[4.25rem] text-very-dark-blue">
          More than just shorter links
        </h1>
        <p className="text-gray">
          Build your brand's recognition and get detailed insights on how your
          links are performing.
        </p>
        <Button className="mt-6 w-fit">Get Started</Button>
      </div>
      <div className='bg-[url("/images/illustration-working.svg")] w-full bg-no-repeat bg-contain bg-center' />
    </div>
  );
};
