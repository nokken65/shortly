import { Link as RouterLink } from '@remix-run/react';

const FEATURES: Array<Link> = [
  { label: 'Link Shortening', url: '/' },
  { label: 'Branded Links', url: '/' },
  { label: 'Analytics', url: '/' },
];

const RESOURCES: Array<Link> = [
  { label: 'Blog', url: '/' },
  { label: 'Developers', url: '/' },
  { label: 'Support', url: '/' },
];

const COMPANY: Array<Link> = [
  { label: 'About', url: '/' },
  { label: 'Our Team', url: '/' },
  { label: 'Careers', url: '/' },
  { label: 'Contact', url: '/' },
];

type Link = {
  label: string;
  url: string;
};

type ListProps = {
  title: string;
  items: Array<Link>;
};

const List = ({ title, items }: ListProps) => (
  <ul className="flex flex-col gap-1 text-gray">
    <li>
      <h2 className="mb-3 text-xl font-bold text-white">{title}</h2>
    </li>
    {items.map((link, index) => (
      <li key={link.label + index}>
        <RouterLink className="link" to={link.url}>
          {link.label}
        </RouterLink>
      </li>
    ))}
  </ul>
);

export const Footer = () => {
  return (
    <footer className="w-full text-white bg-very-dark-violet">
      <div className="container flex justify-between w-full pt-20 pb-20 ml-auto mr-auto">
        <h1 className="text-5xl font-bold">Shortly</h1>
        <div className="flex gap-20">
          <List title="Features" items={FEATURES} />
          <List title="Resources" items={RESOURCES} />
          <List title="Company" items={COMPANY} />
        </div>
        <div className="flex gap-6"></div>
      </div>
    </footer>
  );
};
