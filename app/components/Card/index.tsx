import clsx from 'clsx';

type CardProps = {
  title: string;
  description: string;
  imgUrl: string;
  className?: string;
};

export const Card = ({ title, description, imgUrl, className }: CardProps) => {
  return (
    <div
      className={clsx(
        'h-fit relative p-8 bg-white rounded-md pt-14',
        className
      )}
    >
      <h2 className="mb-3 text-xl font-bold text-very-dark-blue">{title}</h2>
      <p className="text-base text-gray">{description}</p>
      <div className="absolute flex w-20 h-20 rounded-full left-7 -top-10 bg-dark-violet">
        <img
          className="absolute w-10 h-10 -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2"
          src={imgUrl}
          alt={title}
        />
      </div>
    </div>
  );
};
