import Button from '../Button';

export const CallToAction = () => {
  return (
    <div className="flex flex-col items-center w-full gap-10 pt-14 pb-14 bg-dark-violet bg-[url('/images/bg-boost-desktop.svg')] bg-no-repeat bg-cover bg-center">
      <h2 className="text-4xl font-bold text-white">Boost your links today</h2>
      <Button className="text-2xl">Get Started</Button>
    </div>
  );
};
