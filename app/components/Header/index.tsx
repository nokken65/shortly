import { Link } from '@remix-run/react';
import Button from '../Button';

export const Header = () => {
  return (
    <header className="container flex justify-between w-full gap-2 pt-6 pb-6">
      <h1 className="text-4xl font-bold text-dark-violet mr-9">Shortly</h1>
      <nav className="mr-auto">
        <ul className="flex items-center h-full gap-6">
          <li>
            <Link className="nav-link" to="/">
              Features
            </Link>
          </li>
          <li>
            <Link className="nav-link" to="/">
              Prices
            </Link>
          </li>
          <li>
            <Link className="nav-link" to="/">
              Resources
            </Link>
          </li>
        </ul>
      </nav>
      <Button className="pt-2 pb-2 bg-transparent text-grayish-violet hover:brightness-100 hover:bg-cyan hover:text-white">
        Login
      </Button>
      <Button className="pt-2 pb-2 bg-transparent text-grayish-violet hover:brightness-100 hover:bg-cyan hover:text-white">
        Sign Up
      </Button>
    </header>
  );
};
