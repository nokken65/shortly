import type { HTMLAttributes, PropsWithChildren } from 'react';
import clsx from 'clsx';

type ButtonProps = PropsWithChildren<HTMLAttributes<HTMLButtonElement> & {}>;

const Button = ({ children, className, ...props }: ButtonProps) => {
  return (
    <button className={clsx('btn', className)} {...props}>
      {children}
    </button>
  );
};

export default Button;
